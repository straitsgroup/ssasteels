<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PagesController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('PagesModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->PagesModel->getAll('pages'); 
			$data['pagetitle'] = 'Pages List';
			$this->load->view('pages/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$data = array(
					'title' =>$this->input->post('title') , 
					'description' =>$this->input->post('description') , 
					'seo_title' =>$this->input->post('seo_title') , 
					'seo_description' =>$this->input->post('seo_description') , 
					'seo_keyword' =>$this->input->post('seo_keyword') , 
					'page_slug' =>$this->input->post('page_slug') , 
					'cannonical_link' =>$this->input->post('cannonical_link') , 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->PagesModel->add($data, 'pages'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
				}

				redirect(base_url().'admin/pages/add');

			}else{
				$data['pagetitle'] = 'Add Page';
				$this->load->view('pages/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$data = array(
					'title' =>$this->input->post('title') , 
					'description' =>$this->input->post('description') , 
					'seo_title' =>$this->input->post('seo_title') , 
					'seo_description' =>$this->input->post('seo_description') , 
					'seo_keyword' =>$this->input->post('seo_keyword') , 
					'page_slug' =>$this->input->post('page_slug') , 
					'cannonical_link' =>$this->input->post('cannonical_link') , 
					'status' =>$this->input->post('status') , 
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 

				);
				if ($this->PagesModel->edit($data, 'pages', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/pages/list');

			}else{
				$data['pagetitle'] = 'Edit Page';
				$data['Record'] = $this->PagesModel->getById('pages', $id);
				$this->load->view('pages/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->PagesModel->delete('pages', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/pages/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->PagesModel->enable('pages', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/pages/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->PagesModel->disable('pages', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/pages/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
