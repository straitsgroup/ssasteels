<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GalleryController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('GalleryModel');
		$this->load->model('ServiceModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->GalleryModel->getAll('gallery'); 
			$data['pagetitle'] = 'Gallery List';
			$this->load->view('gallery/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/gallery/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('gallery_image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					
					'service_id' =>$this->input->post('service_id') , 
					
					
					'status' =>$this->input->post('status') , 
					'gallery_image' =>$picture , 
					
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->GalleryModel->add($data, 'gallery'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
					redirect(base_url().'admin/gallery/list');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
					redirect(base_url().'admin/gallery/add');
				}


			}else{
				$data['pagetitle'] = 'Add Gallery Image';
				$data['Records'] = $this->ServiceModel->getAll('services');
				$this->load->view('gallery/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	// public function edit($id)
	// {
	// 	if ($this->session->userdata('logged_in')) {
	// 		if($this->input->post()){
	// 			$config['upload_path'] = FCPATH .'/uploads/service-components/';
	// 			$config['allowed_types'] = 'jpeg|jpg|png|gif';
	// 			$config['max_size'] = 4096;
	// 			$this->load->library('upload', $config);
	// 			$this->upload->initialize($config);

	// 			if ($this->upload->do_upload('component_image'))
	// 			{
	// 				$picture = $this->upload->data()['file_name'];
	// 				$data = array(
	// 				'component_title' =>$this->input->post('component_title') , 
	// 				'status' =>$this->input->post('status') , 
	// 				'description' =>$this->input->post('description') ,
	// 				'component_image' =>$picture , 
	// 				'component_metatitle' =>$this->input->post('component_metatitle') , 
	// 				'component_metadesc' =>$this->input->post('component_metadesc') , 
	// 				'component_metakeyword' =>$this->input->post('component_metakeyword') , 
	// 				'slug' =>$this->input->post('slug') , 
	// 				'component_schema' =>$this->input->post('component_schema') , 
	// 				'updated_at'=>date("Y-m-d H:i:s"),
	// 				'updated_by' => $this->session->userdata('username'), 
	// 			);
	// 			}
	// 			else
	// 			{
	// 				$data = array(
	// 				'component_title' =>$this->input->post('component_title') , 
	// 				'status' =>$this->input->post('status') , 
	// 				'description' =>$this->input->post('description') ,
	// 				'component_metatitle' =>$this->input->post('component_metatitle') , 
	// 				'component_metadesc' =>$this->input->post('component_metadesc') , 
	// 				'component_metakeyword' =>$this->input->post('component_metakeyword') , 
	// 				'slug' =>$this->input->post('slug') , 
	// 				'component_schema' =>$this->input->post('component_schema') , 
	// 				'updated_at'=>date("Y-m-d H:i:s"),
	// 				'updated_by' => $this->session->userdata('username'), 

	// 			);
	// 			}
	// 			if ($this->ServiceComponentModel->edit($data, 'servicecomponents', $id))
	// 			{
	// 				$this->session->set_flashdata('msg', 'Record Edited Successfully');
	// 			}
	// 			else
	// 			{
	// 				$this->session->set_flashdata('msg', 'Error Editing Record');
	// 			}
	// 			redirect(base_url().'admin/service-component/list');

	// 		}else{
	// 			$data['pagetitle'] = 'Edit Service Component';
	// 			$data['Record'] = $this->ServiceComponentModel->getById('servicecomponents', $id);
	// 			$data['services'] = $this->ServiceModel->getAll('services');
	// 			$this->load->view('servicecomponents/edit', $data);			
	// 		}
	// 	}else{
	// 		redirect(base_url().'login');
	// 	}
	// }

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->GalleryModel->delete('gallery', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/gallery/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->GalleryModel->enable('gallery', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/gallery/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->GalleryModel->disable('gallery', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/gallery/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
