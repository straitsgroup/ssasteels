<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FaqController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('FaqModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->FaqModel->getAll('faqs'); 
			$data['pagetitle'] = 'FAQ List';
			$this->load->view('faq/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$data = array(
					'question' =>$this->input->post('question') , 
					'answer' =>$this->input->post('answer') , 					
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->FaqModel->add($data, 'faqs'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
					redirect(base_url().'admin/faq/list');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
					redirect(base_url().'admin/faq/add');
				}


			}else{
				$data['pagetitle'] = 'Add FAQ';
				$this->load->view('faq/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$data = array(
					'question' =>$this->input->post('question') , 
					'answer' =>$this->input->post('answer') , 					
					'status' =>$this->input->post('status') , 
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 

				);
				if ($this->FaqModel->edit($data, 'faqs', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/faq/list');

			}else{
				$data['pagetitle'] = 'Edit FAQ';
				$data['Record'] = $this->FaqModel->getById('faqs', $id);
				$this->load->view('faq/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->FaqModel->delete('faqs', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/faq/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->FaqModel->enable('faqs', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/faq/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->FaqModel->disable('faqs', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/faq/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
