<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServicesController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('ServiceModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->ServiceModel->getAll('services'); 
			$data['pagetitle'] = 'Services List';
			$this->load->view('services/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/services/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('service_image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					'service_title' =>$this->input->post('service_title') , 
					
					'description' =>$this->input->post('description') , 
					'status' =>$this->input->post('status') , 
					'service_image' =>$picture , 
					'service_metatitle' =>$this->input->post('service_metatitle') , 
					'service_metadesc' =>$this->input->post('service_metadesc') , 
					'service_metakeyword' =>$this->input->post('service_metakeyword') , 
					'slug' =>$this->input->post('slug') , 
					'service_schema' =>$this->input->post('service_schema') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->ServiceModel->add($data, 'services'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
					redirect(base_url().'admin/service/list');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
					redirect(base_url().'admin/service/add');
				}


			}else{
				$data['pagetitle'] = 'Add Service';
				$this->load->view('services/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/services/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('service_image'))
				{
					$picture = $this->upload->data()['file_name'];
					$data = array(
					'service_title' =>$this->input->post('service_title') , 
					'status' =>$this->input->post('status') , 
					'description' =>$this->input->post('description') ,
					'service_image' =>$picture , 
					'service_metatitle' =>$this->input->post('service_metatitle') , 
					'service_metadesc' =>$this->input->post('service_metadesc') , 
					'service_metakeyword' =>$this->input->post('service_metakeyword') , 
					'slug' =>$this->input->post('slug') , 
					'service_schema' =>$this->input->post('service_schema') , 
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 
				);
				}
				else
				{
					$data = array(
					'service_title' =>$this->input->post('service_title') , 
					'status' =>$this->input->post('status') , 
					'description' =>$this->input->post('description') ,
					'service_metatitle' =>$this->input->post('service_metatitle') , 
					'service_metadesc' =>$this->input->post('service_metadesc') , 
					'service_metakeyword' =>$this->input->post('service_metakeyword') , 
					'slug' =>$this->input->post('slug') , 
					'service_schema' =>$this->input->post('service_schema') , 
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 

				);
				}
				if ($this->ServiceModel->edit($data, 'services', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/service/list');

			}else{
				$data['pagetitle'] = 'Edit Service';
				$data['Record'] = $this->ServiceModel->getById('services', $id);
				$this->load->view('services/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ServiceModel->delete('services', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/service/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ServiceModel->enable('services', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/service/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ServiceModel->disable('services', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/service/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
