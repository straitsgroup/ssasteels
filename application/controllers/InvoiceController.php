<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('InvoiceModel');
		// $this->load->model('ServiceModel');
		// $this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->InvoiceModel->getAll('invoices'); 
			$data['pagetitle'] = 'Invoice List';
			$this->load->view('invoice/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function view($id)
	{
		if ($this->session->userdata('logged_in')) {
			$data['Record'] = $this->InvoiceModel->getById('invoices', $id);			
			$data['pagetitle'] = 'Invoice';			
			$this->load->view('invoice/view', $data);			
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				// $config['upload_path'] = FCPATH .'/uploads/service-components/';
				// $config['allowed_types'] = 'jpeg|jpg|png|gif';
				// $config['max_size'] = 4096;
				// $this->load->library('upload', $config);
				// $this->upload->initialize($config);

				// if ($this->upload->do_upload('component_image'))
				// {
				// 	$picture = $this->upload->data()['file_name'];
				// }
				// else
				// {
				// 	$picture = NULL;
				// }
				$width = $this->input->post('building_width');
				$length = $this->input->post('building_length');
				$height = $this->input->post('building_height');
				$total_price = $this->calculator($width, $length, $height);
				$supply_price = $total_price*0.88;
				$erection_price = $total_price*0.12;

				$data = array(
					'customer_name' =>$this->input->post('customer_name') , 
					'customer_phone' =>$this->input->post('customer_phone') ,
					
					'building_width' =>$this->input->post('building_width') , 
					
					
					'building_length' =>$this->input->post('building_length') , 
					'building_height' =>$this->input->post('building_height') , 
					'location' =>$this->input->post('location') ,
					'supply_price' => $supply_price,
					'erection_price' => $erection_price,
					'total_price' => $total_price,
					
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->InvoiceModel->add($data, 'invoices'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
					redirect(base_url().'admin/invoice/list');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
					redirect(base_url().'admin/invoice/add');
				}


			}else{
				$data['pagetitle'] = 'Create Invoice';
				// $data['Records'] = $this->ServiceModel->getAll('services');
				$this->load->view('invoice/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function calculator($width, $length, $height)
	{
		$price = $width*$length*$height*400;
		return $price;
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				// $config['upload_path'] = FCPATH .'/uploads/service-components/';
				// $config['allowed_types'] = 'jpeg|jpg|png|gif';
				// $config['max_size'] = 4096;
				// $this->load->library('upload', $config);
				// $this->upload->initialize($config);

				$width = $this->input->post('building_width');
				$length = $this->input->post('building_length');
				$height = $this->input->post('building_height');
				$total_price = $this->calculator($width, $length, $height);
				$supply_price = $total_price*0.88;
				$erection_price = $total_price*0.12;

				
				$data = array(
					'customer_name' =>$this->input->post('customer_name') , 
					'customer_phone' =>$this->input->post('customer_phone') ,
					
					'building_width' =>$this->input->post('building_width') , 
					
					
					'building_length' =>$this->input->post('building_length') , 
					'building_height' =>$this->input->post('building_height') , 
					'location' =>$this->input->post('location') , 
					'supply_price' => $supply_price,
					'erection_price' => $erection_price,
					'total_price' => $total_price,
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 

				);
				
				if ($this->InvoiceModel->edit($data, 'invoices', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/invoice/list');

			}else{
				$data['pagetitle'] = 'Edit Invoice';
				$data['Record'] = $this->InvoiceModel->getById('invoices', $id);
				// $data['services'] = $this->ServiceModel->getAll('services');
				$this->load->view('invoice/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->InvoiceModel->delete('invoices', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/invoice/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->InvoiceModel->enable('invoices', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/invoice/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->InvoiceModel->disable('invoices', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/invoice/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function downloadpdf(){
        $this->load->view('welcome_message');
        
        // Get output html
        $html = $this->output->get_output();
        
        // Load pdf library
        $this->load->library('pdf');
        
        // Load HTML content
        $this->dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');
        
        // Render the HTML as PDF
        $this->dompdf->render();
        
        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("welcome.pdf", array("Attachment"=>0));
    }

}
