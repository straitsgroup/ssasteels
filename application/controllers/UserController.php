<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->UserModel->getAll('user'); 
			$data['pagetitle'] = 'User List';
			$this->load->view('user/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$password = hash('sha512', $this->security->xss_clean($this->input->post('password')));

				$data = array(
					'username' =>$this->input->post('username') , 
					'email' =>$this->input->post('email') , 
					'password' => $password, 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),

				);
				if ($this->UserModel->add($data, 'user'))
				{
					$this->session->set_flashdata('msg', 'User Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
				}

				redirect(base_url().'admin/user/add');

			}else{
				$data['pagetitle'] = 'Add User';
				$this->load->view('user/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$data = array(
					'username' =>$this->input->post('username') , 
					'email' =>$this->input->post('email') , 
					'status' =>$this->input->post('status') , 
				);
				if ($this->UserModel->edit($data, 'user', $id))
				{
					$this->session->set_flashdata('msg', 'User Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing User');
				}
				redirect(base_url().'admin/user/list');

			}else{
				$data['pagetitle'] = 'Edit User';
				$data['Record'] = $this->UserModel->getById('user', $id);
				$this->load->view('user/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->UserModel->delete('user', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/user/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->UserModel->enable('user', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/user/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->UserModel->disable('user', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/user/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function changePassword()
	{
		if ($this->session->userdata('logged_in'))
		{
			if($this->input->post())
			{
				$password = hash('sha512', $this->security->xss_clean($this->input->post('password')));

				$data = array(
					'password' => $password, 
				);
				if ($this->UserModel->edit($data, 'user', $this->session->userdata('userid')))
				{
					$this->session->set_flashdata('msg', 'New Password Created Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Updating Password');
				}
				redirect(base_url().'admin/dashboard');

			}else{
				$data['pagetitle'] = 'Change Password';
				$data['Record'] = $this->UserModel->getById('user',$this->session->userdata('userid') );
				$this->load->view('user/changepassword', $data);			
			}
		}
		else
		{
			redirect(base_url());
		}
	}

}
