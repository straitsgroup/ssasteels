<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OtherServicesController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('OtherServicesModel');
		$this->load->model('ServiceModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->OtherServicesModel->getAll('otherservices'); 
			
			$data['pagetitle'] = 'Other Services List';
			$this->load->view('otherservices/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/otherservices/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('otherservices_image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					'otherservices_title' =>$this->input->post('otherservices_title') , 
					'service_id' =>$this->input->post('service_id') , 
					
					'description' =>$this->input->post('description') , 
					'status' =>$this->input->post('status') , 
					'otherservices_image' =>$picture , 
					'otherservices_metatitle' =>$this->input->post('otherservices_metatitle') , 
					'otherservices_metadesc' =>$this->input->post('otherservices_metadesc') , 
					'otherservices_metakeyword' =>$this->input->post('otherservices_metakeyword') , 
					'slug' =>$this->input->post('slug') , 
					'otherservices_schema' =>$this->input->post('otherservices_schema') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->OtherServicesModel->add($data, 'otherservices'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
					redirect(base_url().'admin/otherservices/list');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
					redirect(base_url().'admin/otherservices/add');
				}


			}else{
				$data['pagetitle'] = 'Add Service Component';
				$data['Records'] = $this->ServiceModel->getAll('services');
				$this->load->view('otherservices/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/otherservices/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('otherservices_image'))
				{
					$picture = $this->upload->data()['file_name'];
					$data = array(
					'otherservices_title' =>$this->input->post('otherservices_title') , 
					'status' =>$this->input->post('status') , 
					'description' =>$this->input->post('description') ,
					'otherservices_image' =>$picture , 
					'otherservices_metatitle' =>$this->input->post('otherservices_metatitle') , 
					'otherservices_metadesc' =>$this->input->post('otherservices_metadesc') , 
					'otherservices_metakeyword' =>$this->input->post('otherservices_metakeyword') , 
					'slug' =>$this->input->post('slug') , 
					'otherservices_schema' =>$this->input->post('otherservices_schema') , 
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 
				);
				}
				else
				{
					$data = array(
					'otherservices_title' =>$this->input->post('otherservices_title') , 
					'status' =>$this->input->post('status') , 
					'description' =>$this->input->post('description') ,
					'otherservices_metatitle' =>$this->input->post('otherservices_metatitle') , 
					'otherservices_metadesc' =>$this->input->post('otherservices_metadesc') , 
					'otherservices_metakeyword' =>$this->input->post('otherservices_metakeyword') , 
					'slug' =>$this->input->post('slug') , 
					'otherservices_schema' =>$this->input->post('otherservices_schema') , 
					'updated_at'=>date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('username'), 

				);
				}
				if ($this->OtherServicesModel->edit($data, 'otherservices', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/otherservices/list');

			}else{
				$data['pagetitle'] = 'Edit Other Service';
				$data['Record'] = $this->OtherServicesModel->getById('otherservices', $id);
				$data['services'] = $this->ServiceModel->getAll('services');
				$this->load->view('otherservices/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->OtherServicesModel->delete('servicecomponents', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/service-component/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->OtherServicesModel->enable('servicecomponents', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/service-component/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->OtherServicesModel->disable('servicecomponents', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/service-component/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
