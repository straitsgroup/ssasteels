<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('FrontendModel');
	}
	
	public function index()	{
		$data['seo_title'] = "Home";
		$data['seo_description'] = "Home";
		$data['seo_keywords'] = "Home";
		$data['cannonical_link'] = base_url();
		$data['services'] = $this->FrontendModel->getAll('services');
		$data['gallery'] = $this->FrontendModel->getAll('gallery');
		// print_r($data['gallery']);
		$this->load->view('frontend/home', $data);
	}

	public function gallery()	{
		$data['seo_title'] = "Gallery";
		$data['seo_description'] = "Gallery";
		$data['seo_keywords'] = "Gallery";
		$data['cannonical_link'] = base_url('gallery');
		$data['services'] = $this->FrontendModel->getAll('services');
		$data['gallery'] = $this->FrontendModel->getGallery('gallery');
		// echo "<pre>";print_r($data['services']);
		// echo "<pre>";print_r($data['gallery']);
		$galleryArr = [];
		foreach ($data['gallery'] as $key => $value) {
			$galleryArr[$value['service_id']][] = $value['gallery_image'];
		}
		$newArr = [];
		foreach ($galleryArr as $key => $value) {
			$newArr[$key] = array_chunk($value, 2);
		}
		$data['galleryArr'] = $newArr;
		// echo "<pre>";print_r($galleryArr);
		// echo "<pre>";print_r($newArr);exit;
		$this->load->view('frontend/gallery', $data);
	}

	public function services()	{
		$data['seo_title'] = "Services";
		$data['seo_description'] = "Services";
		$data['seo_keywords'] = "Services";
		$data['cannonical_link'] = base_url('services');
		$data['services'] = $this->FrontendModel->getAll('services');
		// $data['gallery'] = $this->FrontendModel->getAll('gallery');
		// print_r($data['gallery']);
		$this->load->view('frontend/services', $data);
	}

	public function serviceComponent($slug)	{
		$data['seo_title'] = "Service Component";
		$data['seo_description'] = "Service Component";
		$data['seo_keywords'] = "Service Component";
		$data['cannonical_link'] = base_url('service-component');
		$data['serviceComponents'] = $this->FrontendModel->serviceComponents('servicecomponents', $slug);
		$data['otherservices'] = $this->FrontendModel->otherservices('otherservices', $slug);
		// print_r($data['serviceComponents']);
		// $data['slug'] = $slug;
		// $data['gallery'] = $this->FrontendModel->getAll('gallery');
		// print_r($data['gallery']);
		$this->load->view('frontend/servicedetails/'.$slug, $data);
	}

	public function aboutUs()	{
		$data['seo_title'] = "About us";
		$data['seo_description'] = "About us";
		$data['seo_keywords'] = "About us";
		$data['cannonical_link'] = base_url('about-us');
		// $data['testimonials'] = $this->FrontendModel->getAll('testimonials'); 
		$this->load->view('frontend/aboutus', $data);
	}
	public function contactUs()	{
	if($this->input->post()){
			$this->load->config('email');
			$this->load->library('email');

			$to = $this->config->item('smtp_user');
			// $to = 'jayash.n@straitsresearch.net';
			$from = $this->input->post('email');
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject("Contact Us From: Enquiry");

			$maildata = "
			Hello There E- Mail For You,<br>
			<table>
			<tr>
			<td><b>Name</b> : </td>
			<td>".$this->input->post('name')."</td>
			</tr>
			<tr>
			<td><b>Email</b> : </td>
			<td>".$this->input->post('email')."</td>
			</tr>
			<tr>
			<td><b>Phone</b> : </td>
			<td>".$this->input->post('phone')."</td>
			</tr>
			<tr>
			<td><b>Message</b> : </td>
			<td>".$this->input->post('msg')."</td>
			</tr>
			</table>";


			$this->email->set_newline("\r\n");
			$this->email->message($maildata);

			if($this->email->send()) 
				$this->session->set_flashdata("email_sent","Thank you for contacting us."); 
			else 
				$this->session->set_flashdata("email_sent_err","Error in sending Email."); 

			redirect($_SERVER['HTTP_REFERER']);

		}else{

		$data['seo_title'] = "Contact Us";
		$data['seo_description'] = "Contact Us";
		$data['seo_keywords'] = "Contact Us";
		$data['cannonical_link'] = base_url('contact-us');
		$this->load->view('frontend/contactus', $data);
		}			
		
	}

	public function faqs()	{
		$data['seo_title'] = "Faqs";
		$data['seo_description'] = "Faqs";
		$data['seo_keywords'] = "Faqs";
		$data['cannonical_link'] = base_url('questions');
		$data['faqs'] = $this->FrontendModel->getAll('faqs');
		// $data['gallery'] = $this->FrontendModel->getAll('gallery');
		// print_r($data['gallery']);
		$this->load->view('frontend/questions', $data);
	}

	public function calculator()
	{
		if($this->input->post()){
			$width = $this->input->post('width');
			$length = $this->input->post('length');
			$height = $this->input->post('height');
			$location = $this->input->post('location');
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');

			$total_price = $this->calculate($width, $length, $height);
			$supply_price = $total_price*0.88;
			$erection_price = $total_price*0.12;

			$data['width'] = $width;
			$data['length'] = $length;
			$data['height'] = $height;
			$data['location'] = $location;
			$data['total'] = $total_price;
			$data['supply_price'] = $supply_price;
			$data['erection_price'] = $erection_price;
			$data['name'] = $name;
			$data['phone'] = $phone;
			$data['flag'] = 1;

		}
		else{
			$data['width'] = '';
			$data['length'] = '';
			$data['height'] = '';
			$data['location'] = '';
			$data['total'] = '';
			$data['supply_price'] = '';
			$data['erection_price'] = '';
			$data['name'] = '';
			$data['phone'] = '';
			$data['flag'] = 0;


			$data['seo_title'] = "Price calculator";
			$data['seo_description'] = "Price calculator";
			$data['seo_keywords'] = "Price calculator";
			$data['cannonical_link'] = base_url('price-calculator');
		// $data['faqs'] = $this->FrontendModel->getAll('faqs');
		// $data['gallery'] = $this->FrontendModel->getAll('gallery');
		// print_r($data['gallery']);
		}
			$this->load->view('frontend/calculator', $data);
	}

	public function calculate($width, $length, $height)
	{
		$price = $width*$length*$height*400;
		return $price;
	}

	/*Product Page*/
	public function Products()	{
		$data['seo_title'] = "Products";
		$data['seo_description'] = "Products";
		$data['seo_keywords'] = "Products";
		$data['cannonical_link'] = base_url('products');

		$data['products'] = $this->FrontendModel->getAll('category','asc'); 
		$data['d_product'] = $this->FrontendModel->getGroup('download','product','asc'); 
		$data['d_brand'] = $this->FrontendModel->getGroup('download','brand','asc'); 
		$this->load->view('frontend/products/productpage', $data);
	}
	/*Product Sub Pages*/
	public function productSubpages($slug){
		$product = $this->FrontendModel->getBySlug('category',$slug); 
		$data['product']=$product;
		$data['seo_title'] = $product['seo_title'];
		$data['seo_description'] = $product['seo_description'];
		$data['seo_keywords'] = $product['seo_keywords'];
		$data['cannonical_link'] = base_url($product['cannonical_link']);
		$this->load->view('frontend/products/subpages', $data);
	}

	/*Automotive Pages*/
	public function Automotive(){
		$automotive = $this->FrontendModel->getBySlug('category','automotive'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$automotive['id']); 

		$data['automotive']=$automotive;
		$data['pagetitle'] = "Automotive";

		$data['seo_title'] = $automotive['seo_title'];
		$data['seo_description'] = $automotive['seo_description'];
		$data['seo_keywords'] = $automotive['seo_keywords'];
		$data['cannonical_link'] = base_url($automotive['cannonical_link']);

		$this->load->view('frontend/automotive/automotivepage', $data);
	}

	/*Automotive Sub Pages*/
	public function automotiveSubpages($slug){
		$automotive = $this->FrontendModel->getBySlug('category','automotive'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$automotive['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['automotive']=$automotive;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$automotive['id']); 
		$data['pagetitle'] = $sub_page['name'];

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/automotive/subpages', $data);
	}

	/*Industrial Pages*/
	public function Industrial(){
		$industrial = $this->FrontendModel->getBySlug('category','industrial'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$industrial['id']); 
		$data['industrial']=$industrial;

		$data['seo_title'] = $industrial['seo_title'];
		$data['seo_description'] = $industrial['seo_description'];
		$data['seo_keywords'] = $industrial['seo_keywords'];
		$data['cannonical_link'] = base_url($industrial['cannonical_link']);
		$this->load->view('frontend/industrial/industrialpage', $data);
	}

	/*Industrial Sub Pages*/
	public function industrialSubpages($slug){
		$industrial = $this->FrontendModel->getBySlug('category','industrial'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$industrial['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['industrial']=$industrial;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$industrial['id']); 

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/industrial/subpages', $data);
	}
	/*E-Bike Sub Pages*/

	public function eBike(){
		$ebike = $this->FrontendModel->getBySlug('category','e-bike'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ebike['id']); 
		$data['ebike']=$ebike;

		$data['seo_title'] = $ebike['seo_title'];
		$data['seo_description'] = $ebike['seo_description'];
		$data['seo_keywords'] = $ebike['seo_keywords'];
		$data['cannonical_link'] = base_url($ebike['cannonical_link']);

		$this->load->view('frontend/ebike/ebikepage', $data);
	}
	/*E-bike Sub Pages*/

	public function eBikeSubpages($slug){
		$ebike = $this->FrontendModel->getBySlug('category','e-bike'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$ebike['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['ebike']=$ebike;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ebike['id']); 

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/ebike/subpages', $data);
	}
	/*UPS Page*/
	public function Ups(){
		$data['pagetitle'] = "UPS";
		$ups = $this->FrontendModel->getBySlug('category','ups'); 
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ups['id']); 
		$data['ups']=$ups;

		$data['seo_title'] = $ups['seo_title'];
		$data['seo_description'] = $ups['seo_description'];
		$data['seo_keywords'] = $ups['seo_keywords'];
		$data['cannonical_link'] = base_url($ups['cannonical_link']);
		$this->load->view('frontend/ups/upspage', $data);
	}

	/*UPS Sub Pages*/

	public function upsSubpages($slug){
		$ups = $this->FrontendModel->getBySlug('category','ups'); 
		$sub_page = $this->FrontendModel->getSubPageBySlug('subcategory','category_id',$ups['id'],$slug); 
		$data['sub_page']=$sub_page;
		$data['ups']=$ups;
		$data['sub_autmtve'] = $this->FrontendModel->getByrefId('subcategory','category_id',$ups['id']); 

		$data['seo_title'] = $sub_page['seo_title'];
		$data['seo_description'] = $sub_page['seo_description'];
		$data['seo_keywords'] = $sub_page['seo_keywords'];
		$data['cannonical_link'] = base_url($sub_page['cannonical_link']);
		$this->load->view('frontend/ups/subpages', $data);
	}

	public function downloadFiles(){
		$data['pagetitle'] = "Download Files";
		$data['downloads'] = $this->FrontendModel->getAll('download','asc'); 
		$this->load->view('frontend/download', $data);
	}


	public function EnqForm(){
		if($this->input->post()){
			$this->load->config('email');
			$this->load->library('email');

			$to = $this->config->item('smtp_user');
			$from = $this->input->post('email');
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject("Enquiry From: Enquiry");

			$maildata = "
			Hello There E- Mail For You,<br>
			<table>
			<tr>
			<td><b>Name</b> : </td>
			<td>".$this->input->post('name')."</td>
			</tr>
			<tr>
			<td><b>Email</b> : </td>
			<td>".$this->input->post('email')."</td>
			</tr>
			<tr>
			<td><b>Phone</b> : </td>
			<td>".$this->input->post('phone')."</td>
			</tr>
			<tr>
			<td><b>Message</b> : </td>
			<td>".$this->input->post('msg')."</td>
			</tr>
			</table>";


			$this->email->set_newline("\r\n");
			$this->email->message($maildata);

			if($this->email->send()) 
				$this->session->set_flashdata("email_sent","Email sent successfully."); 
			else 
				$this->session->set_flashdata("email_sent_err","Error in sending Email."); 

			redirect($_SERVER['HTTP_REFERER']);

		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function filter(){
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$data = $this->FrontendModel->getBranch('branches',$state,$city); 
		echo json_encode($data);
	}
	public function fileDownload()	{
		if ($this->input->post()) {
			$this->load->helper('download');
			$brand =$this->input->post('brand');
			$product = $this->input->post('product');
			if (empty($brand) || empty($product)) {
				redirect($_SERVER['HTTP_REFERER']);
			}
			$data = $this->FrontendModel->getFile('download',$brand,$product); 
			$fileName= $data['files'];
			if ($fileName) {
				$file = FCPATH .'/uploads/download/'. $fileName;
				$data = file_get_contents ( $file );
				force_download ( $fileName, $data );
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	public function productData(){
		$brand = $this->input->post('value');
		$data = $this->FrontendModel->getProduct('download',$brand); 
		echo json_encode($data);
	}
}