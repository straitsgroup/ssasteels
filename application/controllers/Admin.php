<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}
	
	public function login()
	{
		if ($this->session->userdata('logged_in')) {
			redirect(base_url() . 'admin/dashboard');
		} else {
			if ($this->input->post()) {
				
				if ($this->UserModel->Login($this->input->post())) {
					redirect(base_url() . 'admin/dashboard');
				} else {
					$this->session->set_flashdata('msg', 'Invalid Credentials');
					redirect(base_url());
				}
			} else {
				$data['pagetitle'] = 'Admin Login';
				$this->load->view('back/login', $data);
			}
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url().'login');
	}
}
