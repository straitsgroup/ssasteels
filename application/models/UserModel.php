<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function Login($Array)
	{
		$username = $this->security->xss_clean($Array['email']);
		$password = hash('sha512', $this->security->xss_clean($Array['password']));
		// Prep the query
		$this->db->where('email', $username);
		$this->db->where('password', $password);
		
		// Run the query
		$query = $this->db->get('user');
		
		// Let's check if there are any results
		if($query->num_rows() == 1)
		{
			$this->db->where('id', $query->result_array()[0]['id']);
			$this->db->update('user', array('last_login' => date("Y-m-d H:i:s")));

			$newdata = array(
				'userid'  => $query->result_array()[0]['id'],
				'username'  => $query->result_array()[0]['username'],
				'email'  => $query->result_array()[0]['email'],
				'logged_in' => true
			);
			$this->session->set_userdata($newdata);
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getAll($tbl)
	{
		$sql = $this->db->get($tbl);
		if ($sql->num_rows()>0)
		{
			return $sql->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getById($table, $id)
	{
		$this->db->where('id', $id);
		
		$sql= $this->db->get($table);
		if ($sql->num_rows() == 1)
		{
			return $sql->result_array()[0];
		}
		else
		{
			return false;
		}
	}

	public function add($Array, $table)
	{
		if ($this->db->insert($table, $Array))
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
	}

	public function edit($Array, $table, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update($table, $Array))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete($table, $id)
	{
		if ($this->db->delete($table, array('id' => $id)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function enable($table, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update($table, array('status' => 1)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function disable($table, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update($table, array('status' => 0)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}