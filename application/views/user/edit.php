<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			User Management
			<small>Edit User</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/user/list">Pages</a></li>
			<li class="active">Edit User</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post">
						<div class="col-md-12">
							<div class="form-group">
								<label>User Name *</label>
								<input type="text" name="username" class="form-control" placeholder="Enter User Name" value="<?=$Record['username']?>" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Email *</label>
								<input type="email" name="email" class="form-control" value="<?=$Record['email']?>" placeholder="Enter Email" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
					<!-- /.col -->
				</form>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
</div>
<!-- <script>tinymce.init({selector:'textarea',inline_styles : false});</script> -->
<?php $this->load->view('layouts/footer');?>