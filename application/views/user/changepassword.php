<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			User Management
			<small>Change Password</small>
		</h1>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="col-md-6">
					<form method="post">
						<div class="col-md-12">
							<div class="form-group">
								<label>User Name *</label>
								<input type="text" name="username" class="form-control" placeholder="Enter User Name" value="<?=$Record['username']?>" disabled>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Email *</label>
								<input type="email" name="email" class="form-control" placeholder="Enter Email" value="<?=$Record['email']?>" disabled>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Password *</label>
								<input type="password" name="password" id="password" class="form-control" placeholder="Enter Password" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Confirm Password *</label>
								<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Enter Password Again" required>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary" id="password-setdta" disabled="">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('layouts/footer');?>
<script>
	$('#confirm_password').on('keyup', function() {
		var password = document.getElementById("password").value;
		var confirm_password = document.getElementById("confirm_password").value;
		pass_len=password.length;
		if ($(this).val().length==pass_len){
			if (password === confirm_password) {
				document.getElementById("confirm_password").style.borderBottom = "1px solid green";
				$("#password-setdta").removeAttr("disabled");
			}  else {
				alert("Password and confirm password doesn't match");
				document.getElementById("confirm_password").style.borderBottom = "1px solid red";
				$("#password-setdta").attr("disabled");
			}
		}else{
			document.getElementById("confirm_password").style.borderBottom = "1px solid red";
		}            
	});
</script>