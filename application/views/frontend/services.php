<?php $this->load->view('frontend/layouts/header');?>
<section class="mt100 bg-light mb-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 pt-4">
					<a href="<?=base_url();?>"><span>Home </span></a> <span class="pl-2">/</span> <span class="pl-2 clr-grey">Services</span>
				</div>
				<div class="col-lg-12 text-center pb-4">
					<h2 class="clr-blue pl-2 pt-2 text-uppercase f40">We are known for expertise in</h2>
				</div>
			</div>
		</div>
	</section>
	<section class="py-5 text-center">
		<div class="container my-3">
			<div class="row mt-5">
				<?php foreach ($services as $service): ?>
				<div class="col-lg-4 mob-mb">
					<div class="tringle-link mb-3" id="polygon<?= $service['id'] ?>">
						<a class="ancr-link" id="link<?= $service['id'] ?>">
							<img src="uploads/services/<?= $service['service_image'] ?>" class="img-fluid pl-2 pt-3"><br>
							<span class="tri-link mt-3"><?= $service['service_title'] ?></span><br>
							<span class="f13"><?= $service['description'] ?></span>
						</a>
					</div>
					<a href="<?=base_url();?>services/<?= $service['slug'] ?>" class="btn btn-more" id="link1<?= $service['id'] ?>">View More</a>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</section>	

	<section class="mt-5 pb-5" id="expertise">
		<div class="container">
			<h2 class="text-uppercase clr-blue text-center f40 pt-5">Why SSA Steels ?</h2>
			<div class="row mt-5">
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/value.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Personalized Services</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/clock.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">On Time Delivery</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/support.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Reliable After Sales Services</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/member.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Own Erection Crew (No Subcontracting)</p>
				</div>

				<div class="col-lg-6 d-flex mb-4">
					<img src="images/experience.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Highly Experienced Engineering Team</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/implement.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Value Engineered Solutions</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/recycle-bin.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Faster Cycle Systems</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/medal.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Stringent Quality Systems</p>
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view('frontend/layouts/footer');?>