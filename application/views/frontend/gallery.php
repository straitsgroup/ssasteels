<?php $this->load->view('frontend/layouts/header');?>
<section class="mt100 bg-light mb-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-4">
				<a href="<?=base_url();?>"><span>Home </span></a> <span class="pl-2">/</span> <span class="pl-2 clr-grey">Gallery</span>
			</div>
			<div class="col-lg-12 text-center pb-4">
				<h2 class="clr-blue pl-2 pt-2 text-uppercase f40">Gallery</h2>
			</div>
		</div>
	</div>
</section>	
<section class="py-5" id="expertise">
	<div class="container">
		<?php foreach ($services as $service): ?>
			<h4 class="clr-blue"><?= $service['service_title'] ?></h4>
			<div class="row">
				<div id="gallery<?= $service['id'] ?>" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">

						<?php foreach ($galleryArr as $service_id => $gallery): ?>
							<?php if ($service_id == $service['id']): ?>
								<?php foreach ($gallery as $key => $imageArr): ?>
									<?php if ($key == 0){ ?>
									<div class="carousel-item active">
									<?php } else { ?>
										<div class="carousel-item">
									<?php } ?>
										<div class="row">
											<?php foreach ($imageArr as $image): ?>
												<div class="col-lg-6 p-5">
													<div class="galley-frame px-5">
														<div class="pdt95">
															<img src="uploads/gallery/<?= $image ?>" alt="banner1" class="img-fluid img-glry">
														</div>
													</div>
												</div>
											<?php endforeach ?>

										</div>
									</div>
								<?php endforeach ?>
							<?php endif ?>
						<?php endforeach ?>
						
					</div>
					<a class="carousel-control-prev" href="#gallery<?= $service['id'] ?>" data-slide="prev">
						<i class="fa fa-caret-left clr-blue f60"></i>
					</a>
					<a class="carousel-control-next" href="#gallery<?= $service['id'] ?>" data-slide="next">
						<i class="fa fa-caret-right clr-blue f60"></i>
					</a>
				</div>				
			</div>
		<?php endforeach ?>

	</div>
</section>
<?php $this->load->view('frontend/layouts/footer');?>