<?php $this->load->view('frontend/layouts/header');?>
<section class="mt100 bg-light mb-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 pt-4">
					<a href="<?=base_url();?>"><span>Home </span></a>  <span class="pl-2">/</span> <a href="<?=base_url();?>services"><span class="pl-2">Services</span></a> <span class="pl-2">/</span><span class="pl-2 clr-grey">PEB Maintenance & Re-Roofing</span>
				</div>
				<div class="col-lg-12 text-center pb-4">
					<h2 class="clr-blue pl-2 pt-2 text-uppercase f40">We are known for expertise in</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="py-5 text-center" id="expertise">
		<div class="container my-3">
			<div class="row">
				<div class="col-lg-4 offset-lg-4 mob-mb">
					<div class="tringle-link mb-3" id="polygon1">
						<a class="ancr-link" id="link1">
							<img src="<?=base_url();?>uploads/services/<?= $serviceComponents[0]['service_image'] ?>" class="img-fluid pl-2 pt-3"><br>
							<span class="tri-link mt-3"><?= $serviceComponents[0]['service_title'] ?></span><br>
							<span class="f13"><?= $serviceComponents[0]['description'] ?></span>
						</a>
					</div>
					<i class="fa fa-caret-down f40 clr-blue"></i>
				</div>
			</div>
		</div>
	</section>

	<section class="pt-5" id="expertise">
		<div class="container">
			<div class="row">
				<?php foreach ($serviceComponents as $component): ?>
				<div class="col-lg-4 mb-5 p-5">
					<div class="galley-frame1 px-5">
						<div class="pdt95">
							<img src="<?=base_url();?>uploads/service-components/<?= $component['component_image'] ?>" alt="banner1" class="img-fluid img-glry">
						</div>
					</div>
					<div class="mt-3 text-center">
						<h4 class="clr-blue"><?= $component['component_title'] ?></h4>
						<p class="mt-3"><?= $component['component_description'] ?></p>
					</div>
				</div>
				<?php endforeach ?>

				
			</div>
		</div>
	</section>
	<?php $this->load->view('frontend/layouts/footer');?>