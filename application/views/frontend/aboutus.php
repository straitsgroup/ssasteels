<?php $this->load->view('frontend/layouts/header');?>
<section class="mt100 bg-light mb-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-4">
				<a href="<?=base_url();?>"><span>Home </span></a>  <span class="pl-2">/</span> <span class="pl-2 clr-grey">About Us</span>
			</div>
			<div class="col-lg-12 text-center pb-4">
				<h2 class="clr-blue pl-2 pt-2 text-uppercase f40">About SSA Steels</h2>
			</div>
		</div>
	</div>
</section>
<section class="py-3 mb-5">
	<div class="container pb-5">
		<div class="row">
			<div class="col-lg-12 text-center">
				<p>
					We are the experts of PRE-ENGINEERED BUILDINGS.
				</p>
				<p>

					Pioneered by Mr. Sanjay Patil, who passionately involves in every function. Be it planning, design, production, erection and CRM of SSA Steels. We have won our customers trust due to: Highly experienced design team, Well equipped in-house erection Crew, State of art PEB manufacturing Unit, customer centric after sales service, premium quality products. 
				</p>
				<p> 

					SSA Steels offers complete solutions to their customers in the design, Engineering, Manufacturing, Supply & Erection of Pre-Engineering Metal Buildings. Alos SSA Steels provide the solution to the existing PEB / Factory building problems & executing the same. SSA Steels, hereby address the full requirements of PEB in house since concept to maintenance. 
				</p>
				 <p>

					SSA Steels emerged with new trend in PEB industry the, MAINTENANCE of PEB & factory houses. With their expert team & huge experience they are handling all types of modification, rectification & maintenance of existing factory building. Offering optimum solution is the best thins with SSA Steels. Among all maintenance activities, Replacement of existing damaged sheets / Asbestos cement sheets are their knack.
				</p>
			</div>
		</div>
	</div>
</section>

<section id="obj-plspy">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 py-5">
				<div class="obj-box mr-5 px-5 py-4 mt-5">
					<h2 class="clr-blue pl-2">Our Objective.</h2>
					<p class="pl-2">To devoid the PEB building industry problems through innovative techniques & applications of versatile.</p>
				</div>
			</div>
			<div class="col-lg-6 py-5">
				<div class="obj-box ml-5 px-5 py-4 my-5">
					<h2 class="clr-blue pl-2">Our Philosophy.</h2>
					<p class="pl-2">To devoid the PEB building industry problems through innovative techniques & applications of versatile.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="my-5">
	<div class="container pt-4">
		<h2 class="clr-blue text-center pb-5 text-uppercase f40">About Team</h2>
		<div class="row">
			<div class="col-lg-5 mb-3">
				<img src="images/about-us.png" class="img-fluid">
			</div>
			<div class="col-lg-7">
				<p>
					SSA Steels is founded & headed by Sanjay Patil. He is Civil Engineering graduate with masters in Management, having 12+ years experienced in Pre-Engineering Steel Building industry.  He exercises strong management skills and has expertise in systems and finance issues.
				</p>
				<p>

					 

					He also has experience in technical sales and solution providing of pre-engineered metal building systems, customer relationship management for sales as well as large scale project management.
				</p>
				<p>
					 

					Sanjay also manages the departmental Business Processes backend and their functions and day to day operations relating to ERP & CRM a systems ensuring then company operations run with a smooth and efficient function.
				</p>
				<p>
					 

					Many other pillers, like Ms. Sae (Administration) Nikhil & Sourav (Projects), Shreenath (Design), Santish (Production), Amit (Finance & Accounts), make SSA Steels to rise. Own highly experienced erection crew is biggest asset SSA Steels has.
				</p>
				
			</div>
		</div>
	</div>
</section>
<section class="mt-5 pb-5" id="expertise">
	<div class="container">
		<h2 class="text-uppercase clr-blue text-center f40 pt-5">Why SSA Steels ?</h2>
		<div class="row mt-5">
			<div class="col-lg-6 d-flex mb-4">
				<img src="images/value.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">Personalized Services</p>
			</div>
			<div class="col-lg-6 d-flex mb-4">
				<img src="images/clock.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">On Time Delivery</p>
			</div>
			<div class="col-lg-6 d-flex mb-4">
				<img src="images/support.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">Reliable After Sales Services</p>
			</div>
			<div class="col-lg-6 d-flex mb-4">
				<img src="images/member.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">Own Erection Crew (No Subcontracting)</p>
			</div>

			<div class="col-lg-6 d-flex mb-4">
				<img src="images/experience.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">Highly Experienced Engineering Team</p>
			</div>
			<div class="col-lg-6 d-flex mb-4">
				<img src="images/implement.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">Value Engineered Solutions</p>
			</div>
			<div class="col-lg-6 d-flex mb-4">
				<img src="images/recycle-bin.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">Faster Cycle Systems</p>
			</div>
			<div class="col-lg-6 d-flex mb-4">
				<img src="images/medal.svg" class="img-fluid wdt50">
				<p class="my-auto f18 pl-5">Stringent Quality Systems</p>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('frontend/layouts/footer');?>