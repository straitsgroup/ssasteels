<?php $this->load->view('frontend/layouts/header');?>
	<header class="text-white text-center">
		<div id="demo" class="carousel slide mt100" data-ride="carousel">
			<ul class="carousel-indicators">
				<li data-target="#demo" data-slide-to="0" class="active"></li>
				<li data-target="#demo" data-slide-to="1"></li>
				<li data-target="#demo" data-slide-to="2"></li>
			</ul>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="images/banner1.jpg" alt="banner1" class="img-fluid">
					<div class="carousel-caption text-center">
						<h2 class="banner-heading text-uppercase">We Don't just Produce Buildings, <br> We serve Them</h2>
					</div>
				</div>
				<div class="carousel-item">
					<img src="images/banner2.jpg" alt="banner2" class="img-fluid">
					<div class="carousel-caption text-center">
						<h2 class="banner-heading text-uppercase">Pre-Engineering Steel Buildings</h2>
						
					</div>
				</div>
				<div class="carousel-item">
					<img src="images/banner3.jpg" alt="banner3" class="img-fluid">
					<div class="carousel-caption text-center">
						<h2 class="banner-heading text-uppercase">Components & Accessories</h2>
						
					</div>
				</div>
			</div>
		</div>
	</header>

	<section class="py-5 bg-light text-center" id="expertise">
		<div class="container my-3">
			<h2 class="text-uppercase clr-blue f40">We are known for expertise in</h2>
			<div class="row mt-5">
				<?php foreach ($services as $service): ?>
				<div class="col-lg-4 mob-mb">
					<div class="tringle-link mb-3" id="polygon<?= $service['id'] ?>">
						<a class="ancr-link" id="link<?= $service['id'] ?>">
							<img src="uploads/services/<?= $service['service_image'] ?>" class="img-fluid pl-2 pt-3"><br>
							<span class="tri-link mt-3"><?= $service['service_title'] ?></span><br>
							<span class="f13"><?= $service['description'] ?></span>
						</a>
					</div>
					<a href="<?=base_url();?>services/<?= $service['slug'] ?>" class="btn btn-more" id="link1<?= $service['id'] ?>">View More</a>
				</div>
				 <?php endforeach ?>
			</div>
		</div>
	</section>

	<section class="my-5">
		<div class="container">
			<h2 class="text-uppercase clr-blue text-center f40">Why SSA Steels ?</h2>
			<div class="row mt-5">
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/value.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Personalized Services</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/clock.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">On Time Delivery</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/support.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Reliable After Sales Services</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/member.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Own Erection Crew (No Subcontracting)</p>
				</div>

				<div class="col-lg-6 d-flex mb-4">
					<img src="images/experience.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Highly Experienced Engineering Team</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/implement.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Value Engineered Solutions</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/recycle-bin.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Faster Cycle Systems</p>
				</div>
				<div class="col-lg-6 d-flex mb-4">
					<img src="images/medal.svg" class="img-fluid wdt50">
					<p class="my-auto f18 pl-5">Stringent Quality Systems</p>
				</div>
			</div>
		</div>
	</section>
	<section id="obj-plspy">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 py-5">
					<div class="obj-box mr-5 px-5 py-4 mt-5">
						<h2 class="clr-blue pl-2">Our Objective.</h2>
						<p class="pl-2">To devoid the PEB building industry problems through innovative techniques & applications of versatile products.</p>
					</div>
				</div>
				<div class="col-lg-6 py-5">
					<div class="obj-box ml-5 px-5 py-4 my-5">
						<h2 class="clr-blue pl-2">Our Philosophy.</h2>
						<p class="pl-2">To enhance the life of Pre-Engineering Building sheds & provide trouble free production space.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="my-5">
		<div class="container">
			<h2 class="text-uppercase clr-blue text-center f40">Gallery</h2>
			<div class="row mt-5">
				<div id="gallery" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="row">
								<div class="col-lg-6 p-5">
									<div class="galley-frame px-5">
										<div class="pdt95">
											<img src="uploads/gallery/<?= $gallery[0]['gallery_image'] ?>" alt="banner1" class="img-fluid img-glry">
										</div>
									</div>
								</div>
								<div class="col-lg-6 p-5">
									<div class="galley-frame px-5">
										<div class="pdt95">
											<img src="uploads/gallery/<?= $gallery[1]['gallery_image'] ?>" alt="banner1" class="img-fluid img-glry">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="row">
								<div class="col-lg-6 p-5">
									<div class="galley-frame px-5">
										<div class="pdt95">
											<img src="uploads/gallery/<?= $gallery[2]['gallery_image'] ?>" alt="banner1" class="img-fluid img-glry">
										</div>
									</div>
								</div>
								<div class="col-lg-6 p-5">
									<div class="galley-frame px-5">
										<div class="pdt95">
											<img src="uploads/gallery/<?= $gallery[3]['gallery_image'] ?>" alt="banner1" class="img-fluid img-glry">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a class="carousel-control-next" href="#gallery" data-slide="next">
						<!-- <span class="carousel-control-next-icon"></span> -->
						<i class="fa fa-caret-right clr-blue f60"></i>
					</a>
				</div>
				<div class="text-center col-lg-12 mt-5">
					<a href="<?=base_url();?>gallery" class="btn btn-more">View More</a>
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view('frontend/layouts/footer');?>