<?php $this->load->view('frontend/layouts/header');?>
<section class="mt100 bg-light mb-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-4">
				<a href="<?=base_url();?>"><span>Home </span></a>  <span class="pl-2">/</span> <span class="pl-2">Resources</span> <span class="pl-2">/</span><span class="pl-2 clr-grey">Price Calculator</span>
			</div>
			<div class="col-lg-12 text-center pb-4">
				<h2 class="clr-blue pl-2 pt-2 text-uppercase f40">Price Calculator For  Standard Box Building</h2>
			</div>
		</div>
	</div>
</section>
<section class="py-3">
	<div class="container pb-5">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form method="post">
					<div class="row">
						<div class="col-lg-6 text-right my-auto mob-py4">
							<p class="cal-label my-auto">Width of the Building </p>
						</div>
						<div class="col-lg-6 text-left d-flex">
							<span class="pr-3 my-auto">-</span>
							<span class="pr-3 my-auto">
								<input type="tel" class="form-control cal-inpt dimensions" placeholder="120" id="width" name="width" required value="<?= $width ?>">
							</span>
							<span class="pr-5 my-auto cal-unit" >Meter</span>
						</div>

						<div class="col-lg-6 text-right my-auto mob-pt4">
							<p class="cal-label my-auto">Length of the Building </p>
						</div>
						<div class="col-lg-6 text-left d-flex py-4">
							<span class="pr-3 my-auto">-</span>
							<span class="pr-3 my-auto">
								<input type="tel" class="form-control cal-inpt dimensions" placeholder="60" id="length" name="length" required value="<?= $length ?>">
							</span>
							<span class="pr-5 my-auto cal-unit" >Meter</span>
						</div>

						<div class="col-lg-6 text-right my-auto pb-4">
							<p class="cal-label my-auto">Height of the Building (Clear) </p>
						</div>
						<div class="col-lg-6 text-left d-flex pb-4">
							<span class="pr-3 my-auto">-</span>
							<span class="pr-3 my-auto">
								<input type="tel" class="form-control cal-inpt dimensions" placeholder="10" id="height" name="height" required value="<?= $height ?>">
							</span>
							<span class="pr-5 my-auto cal-unit" >Meter</span>
						</div>

						<div class="col-lg-6 text-right my-auto pb-4">
							<p class="cal-label my-auto">Customer Name * </p>
						</div>
						<div class="col-lg-6 text-left d-flex pb-4">
							<span class="pr-3 my-auto">-</span>
							<span class="pr-3 pr-5  my-auto">
								<input type="text" class="form-control cal-inpt" placeholder="Name" id="name" name="name" required value="<?= $name ?>">
							</span>
						</div>

						<div class="col-lg-6 text-right my-auto pb-4">
							<p class="cal-label my-auto">Customer Phone * </p>
						</div>
						<div class="col-lg-6 text-left d-flex pb-4">
							<span class="pr-3 my-auto">-</span>
							<span class="pr-3 pr-5  my-auto">
								<input type="text" class="form-control cal-inpt" placeholder="Phone" id="phone" name="phone" required value="<?= $phone ?>" onkeyup = "if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
							</span>
						</div>

						<div class="col-lg-6 text-right my-auto pb-4">
							<p class="cal-label my-auto">Location </p>
						</div>
						<?php if ($flag == 1): ?>
							<div class="col-lg-6 text-left d-flex pb-4" id="displayInvoice">
								<?php else: ?>
									<div class="col-lg-6 text-left d-flex pb-4">
									<?php endif ?>
									<span class="pr-3 my-auto">-</span>
									<span class="pr-3 pr-5  my-auto">
										<input type="text" class="form-control cal-inpt" placeholder="Pune" id="location" name="location" required value="<?= $location ?>">
									</span>
								</div>

								<div class="col-lg-12 text-center mt-3">
									<button type="submit" class="btn cal-btn ml-5">Calculate</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>

		<section class="mb-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 mb-5">
						<table class="table1">
							<thead>
								<tr>
									<th class="br-r">Building</th>
									<th class="br-r">Supply (Rs.)</th>
									<th>Erection (Rs.)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="br-b br-r">Basic Price of Steel Building</td>
									<td class="br-b br-r"><?= $supply_price ?></td>
									<td class="br-b"><?= $erection_price ?></td>
								</tr>
								<tr>
									<td class="br-r"><b>Total Price for Design, Manufacturing, <br> Transportation & Erection of  Steel bldg (INR):</b></td>
									<td colspan="2"><b><?= $total ?></b></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-lg-12 text-center">
						<!-- <a href="#" class="btn cal-btn ml-5">Download</a> -->
						<input type="button" class="btn cal-btn ml-5" onclick="printDiv('quotation')" value="Download" />
					</div>
				</div>
			</div>
		</section>

		<!-- invoice pdf -->
		<section class="mb-5" style="display: none;">
			<div class="container" id="quotation">
				<div class="row">
					<div class="col-lg-12">
						<img src="http://www.enayble.com/demo/ssasteels/images/logo.svg" class="img-fluid">
					</div>
					<div class="col-lg-6">
						<p class="clr-blue fnt-fmly">To, <br>
							MR. <?= $name ?> <br>
							Phone No. <?= $phone ?>
						</p>
					</div>
					<div class="col-lg-6 text-right">
						<p class="clr-blue fnt-fmly">Date : <?= date("d-m-yy") ?></p>
					</div>
					<div class="col-lg-12">
						<h6 class="clr-blue">Subject: <span class="f15 pl-3">Quotation for your requirement of Design, Manufacturing & Installation of Pre-Engineering Building for Standard Box Building at <?= $location ?>.</span></h6>
					</div>
					<div class="col-lg-12 mb-5 mt-3">
						<table class="table2">
							<tbody>
								<tr>
									<td class="br-b br-r">Width</td>
									<td class="br-b"><?= $width ?> M O/O of Steel Line</td>
								</tr>
								<tr>
									<td class="br-b br-r">Length</td>
									<td class="br-b"><?= $length ?> M O/O of Steel Line</td>
								</tr>
								<tr>
									<td class="br-b br-r">Height</td>
									<td class="br-b"><?= $height ?> M Clear Ht</td>
								</tr>
								<tr>
									<td class="br-b br-r">Roof slope / future Expansion</td>
									<td class="br-b">1:10 / Nil</td>
								</tr>
								<tr>
									<td class="br-b br-r">Bracings</td>
									<td class="br-b">Rod Bracings at Roof & Wall</td>
								</tr>
								<tr>
									<td class="br-b br-r">Roofing & Wall Sheet</td>
									<td class="br-b">0.50 MM Pre-Panted Galvanized (PPGI) Sheets (Profiled sheets)</td>
								</tr>
								<tr>
									<td class="br-b br-r">Brick Masonry (by others)</td>
									<td class="br-b">B/W up-to 3.0 m & above sheeting from all 4 sides</td>
								</tr>
								<tr>
									<td class="br-b br-r">Gutter / Down Spouts</td>
									<td class="br-b">Yes. Gutter & D/T pipe considered</td>
								</tr>
								<tr>
									<td class="br-b br-r">Mezzanine / Crane</td>
									<td class="br-b">Nil. (Please contact with details.)</td>
								</tr>
								<tr>
									<td class="br-b br-r">Framed Openings / Canopy</td>
									<td class="br-b">Nil. (Please contact with details.)</td>
								</tr>
								<tr>
									<td class="br-b br-r">Sky Lights</td>
									<td class="br-b">Polycarbonate sheets 2 mm Thk. 3-5 % of Roof area</td>
								</tr>
								<tr>
									<td class="br-b br-r">Turbo Fans</td>
									<td class="br-b">600 mm dia throat. Standard</td>
								</tr>
								<tr>
									<td class="br-b br-r">Design Considerations</td>
									<td class="br-b">Code – AISC / MBMA. With standard loading</td>
								</tr>
								<tr>
									<td class="br-b br-r">Material Specifications</td>
									<td class="br-b">As per Industrial Standard</td>
								</tr>
								<tr>
									<td class="br-r">Schedule</td>
									<td>Can be discussed</td>
								</tr>
							</tbody>
						</table>
					</div>


					<div class="col-lg-12 mb-5 table-responsive">
						<table class="table1">
							<thead>
								<tr>
									<th class="br-r">Building</th>
									<th class="br-r">Supply (Rs.)</th>
									<th>Erection (Rs.)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="br-b br-r">Basic Price of Steel Building</td>
									<td class="br-b br-r"><?= $supply_price ?></td>
									<td class="br-b"><?= $erection_price ?></td>
								</tr>
								<tr>
									<td class="br-r"><b>Total Price for Design, Manufacturing, <br> Transportation & Erection of  Steel bldg (INR):</b></td>
									<td colspan="2"><b><?= $total ?></b></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-lg-12">
						<h4 class="clr-blue">Terms & Conditions: </h4>
						<p class="fnt-fmly clr-blue">
							ALL TAXES ARE EXTRA AT ACTUAL.  <br>
							Estimated price is very much tentative & we reserve all rights to alter the all above depending up-on situation. <br>
						Please contact us for more detailed quote.</p>
					</div>
				</div>
			</div>
		</section>
		<?php $this->load->view('frontend/layouts/footer');?>