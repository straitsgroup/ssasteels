<?php $this->load->view('frontend/layouts/header');?>
<section class="mt100 bg-light mb-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-4">
				<a href="<?=base_url();?>"><span>Home </span></a> <span class="pl-2">/</span> <span class="pl-2 clr-grey">Contact Us</span>
			</div>
			<div class="col-lg-12 text-center pb-4">
				<h2 class="clr-blue pl-2 pt-2 text-uppercase f40">Contact Us</h2>
			</div>
		</div>
	</div>
</section>

<section class="pt-5" id="expertise">
	<div class="container">
		<?php if($this->session->flashdata('email_sent')): ?>
                <div class="alert alert-info">
                    <?php echo $this->session->flashdata('email_sent') ?>
                </div>
            <?php endif ?>
            <?php if($this->session->flashdata('email_sent_err')): ?>
                <div class="alert alert-danger">
                    <strong>Info!</strong> <?php echo $this->session->flashdata('email_sent_err') ?>
                </div>
            <?php endif ?>
		<div class="row">
			<div class="col-lg-6 mob-mb text-center order-lg-first">
				<img src="images/contact-us.png" class="img-fluid">
			</div>
			<div class="col-lg-6 mt-5 order-first">
				<form method="post" class="mr-5 contact-form">
					<div class="form-group pr-5">
						<input type="text" name="name" class="form-control" placeholder="Full Name *" id="name" value = "<?php echo set_value('name'); ?>" required >  
						<?php  if(form_error('name')){echo "<span style='color:red'>".form_error('name')."</span>";} ?>
					</div>
					<div class="form-group pr-5">
						<input type="email" name="email" class="form-control" placeholder="Email Id *" id="email" required="" value = "<?php echo set_value('email'); ?>">
						<?php  if(form_error('email')){echo "<span style='color:red'>".form_error('email')."</span>";} ?>
					</div>
					<div class="form-group pr-5">
						<input type="tel" name="phone" class="form-control" placeholder="Phone Number *" id="phone" value = "<?php echo set_value('phone'); ?>" required="" onkeyup = "if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
						<?php  if(form_error('phone')){echo "<span style='color:red'>".form_error('phone')."</span>";} ?>
					</div>
					<div class="form-group pr-5">
						<textarea class="form-control" name="msg" rows="3" id="message" placeholder="Message *" required=""><?php echo set_value('msg'); ?></textarea>
						<?php  if(form_error('msg')){echo "<span style='color:red'>".form_error('msg')."</span>";} ?>
					</div>
					<div class="form-group text-right mr-5">
						<button type="submit" class="btn submit-btn">SUBMIT</button>
					</div>
				</form>
			</div>

			<div class="col-lg-4 mt-5">
				<div class="row">
					<div class="col-lg-4 text-right">
						<img class="img-fluid" src="images/address.png" alt="Address">
					</div>
					<div class="col-lg-8 pb-2">
						<p class="my-auto">
							Plot No -72/2, 711, <br>
							Ambegaon Khurd,	Pune - 411046.
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 mt-5">
				<div class="row">
					<div class="col-lg-4 text-right pb-2">
						<img class="img-fluid" src="images/contacts.png" alt="Address">
					</div>
					<div class="col-lg-8 pb-2">
						<p class="my-auto pt-2">
							+91 9561145007
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 mt-5">
				<div class="row">
					<div class="col-lg-4 text-right">
						<img class="img-fluid" src="images/mail.png" alt="Address">
					</div>
					<div class="col-lg-8">
						<p class="">
							info@ssasteels.in
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<?php $this->load->view('frontend/layouts/footer');?>