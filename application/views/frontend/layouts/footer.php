<footer class="footer ftr-bg pt-5">
	<div class="container">
		<div class="row">	
			<div class="col-lg-4 mob-mb">
				<img src="<?=base_url();?>images/logo-white.svg" class="logo img-fluid">
			</div>				
			<div class="col-lg-3 mob-mb">
				<ul class="list-unstyled">
					<li><a href="<?=base_url();?>" class="footer-link">Home</a></li>
					<li><a href="<?=base_url();?>about-us" class="footer-link">About SSA Steel</a></li>
					<li><a href="<?=base_url();?>services" class="footer-link">Services</a></li>
					<li><a href="<?=base_url();?>gallery" class="footer-link">Gallery</a></li>
					<li><a href="#" class="footer-link">Resources</a></li>
					<li><a href="<?=base_url();?>contact-us" class="footer-link">Contact Us</a></li>
				</ul>
			</div>				
			<div class="col-lg-5 mob-mb">
				<div class="row">
					<div class="col-lg-4 text-right">
						<img class="img-fluid" src="<?=base_url();?>images/address.svg" alt="Address">
					</div>
					<div class="col-lg-8 pb-3">
						<p class="clr-white my-auto">
							Plot No -72/2, 711, <br>
							Ambegaon Khurd,	Pune - 411046.
						</p>
					</div>
					<div class="col-lg-4 text-right pb-3">
						<img class="img-fluid" src="<?=base_url();?>images/contacts.svg" alt="Address">
					</div>
					<div class="col-lg-8 pb-3">
						<p class="clr-white my-auto">
							+91 9561145007
						</p>
					</div>
					<div class="col-lg-4 text-right">
						<img class="img-fluid" src="<?=base_url();?>images/mail.svg" alt="Address">
					</div>
					<div class="col-lg-8">
						<p class="clr-white">
							info@ssasteels.in
						</p>
					</div>
				</div>
			</div>				
		</div>
	</div>
	<hr class="ftr-hr">
	<div class="container">
		<div class="row text-center">
			<div class="col-lg-12">
				<p class="copyright"> &copy; All Rights Reserved By SSA Steels | Powered By <a href="https://www.enayble.com/" target="_blank" class="enayble">Enayble.com</a></p>
			</div>
		</div>				
	</div>
</footer>
<script src="<?=base_url();?>vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url();?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url();?>js/custom.js"></script>
<script src="<?=base_url();?>js/printThis.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		var url = window.location;
		$('li.navbar-nav a[href="'+ url +'"]').parent().addClass('active');
		$('ul.navbar-nav a').filter(function() {
			return this.href == url;
		}).parent().addClass('active');

	});
</script>
<script type="text/javascript">
	function printDiv(divName) {
		bprint()
		$('#'+divName).printThis({
			pageTitle: "SSA Steel Invoice",
			// beforePrint: bprint(), 
			// afterPrint: aprint() 
		});
	
	}
	function bprint() {
		$("#site_title").html("SSA Steel Invoice");
	}
	

	$('.dimensions').keypress(function(event) {
		if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});

	$("#phone").on("blur", function(){
		var mobNum = $(this).val();
		var filter = /^\d*(?:\.\d{1,2})?$/;

		if (filter.test(mobNum)) {
			if(mobNum.length==10){
                  // alert("valid");
              // $("#mobile-valid").removeClass("hidden");
              // $("#folio-invalid").addClass("hidden");
          } else {
          	alert('Please enter valid 10 digit mobile number');
          	$(this).val('')
               // $("#folio-invalid").removeClass("hidden");
               // $("#mobile-valid").addClass("hidden");
               return false;
           }
       }
       else {
              // alert('Not a valid number');
              $(this).val('')
              // $("#folio-invalid").removeClass("hidden");
              // $("#mobile-valid").addClass("hidden");
              return false;
          }

      });
  </script>
  <script type="text/javascript">
  	$(document).ready(function () {

  		$('html, body').animate({
  			scrollTop: $("#displayInvoice").offset().top
  		}, 1000);
  	});
  </script>
</body>
</html>
