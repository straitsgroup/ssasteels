<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/png" href="<?=base_url();?>images/logo.svg">
	<title id="site_title">Home Page | SSA Steels </title>
	<meta name="description" content="">
	<meta name="author" content="">
	<link href="<?=base_url();?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url();?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
	<link href="<?=base_url();?>vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url();?>css/custom.css" rel="stylesheet">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top py-2">
		<div class="container">
			<a class="navbar-brand" href="<?=base_url();?>">
				<img src="<?=base_url();?>images/logo.svg" class="img-fluid logo">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="btn nav-link btn-menu" href="<?=base_url();?>">Home </a>
						<img src="<?=base_url();?>images/home-img.png" class="img-fluid onactive mob-hide">
					</li>
					<li class="nav-item">
						<a class="btn nav-link btn-menu" href="<?=base_url();?>about-us">About Us </a>
						<img src="<?=base_url();?>images/home-img.png" class="img-fluid onactive mob-hide">
					</li>

					<li class="">
						<div class="dropdown nav-drpdwn">
							<a href="<?=base_url();?>services" class="btn nav-link btn-menu" >Services</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="<?=base_url();?>services/pre-engineering-steel-buldings">Pre-Engineering Steel Buldings</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?=base_url();?>services/accessories-metal-roofing-wall-cladding">PEB Components and Accessories</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?=base_url();?>services/peb-maintenance-roofing">PEB Maintenance Re-Roofing</a>
							</div>
							<img src="<?=base_url();?>images/home-img.png" class="img-fluid onactive mob-hide">
						</div>
					</li>
					<li class="">
						<div class="dropdown nav-drpdwn">
							<a class="btn nav-link btn-menu" >Resources</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="<?=base_url();?>questions">Questions</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?=base_url();?>price-calculator">Price Calculator</a>
							</div>
							<img src="<?=base_url();?>images/home-img.png" class="img-fluid onactive mob-hide">
						</div>
					</li>
					
					<li class="nav-item">
						<a class="btn nav-link btn-menu" href="<?=base_url();?>gallery">Gallery </a>
						<img src="<?=base_url();?>images/home-img.png" class="img-fluid onactive mob-hide">
					</li>
					<li class="nav-item">
						<a class="btn nav-link btn-menu" href="<?=base_url();?>contact-us">Contact Us</a>
						<img src="<?=base_url();?>images/home-img.png" class="img-fluid onactive mob-hide">
					</li>
				</ul>
			</div>
		</div>
	</nav>
