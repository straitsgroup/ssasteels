<?php $this->load->view('frontend/layouts/header');?>
	<section class="mt100 bg-light mb-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 pt-4">
					<a href="<?=base_url();?>"><span>Home </span></a>  <span class="pl-2">/</span> <span class="pl-2">Resources</span> <span class="pl-2">/</span><span class="pl-2 clr-grey">Questions</span>
				</div>
				<div class="col-lg-12 text-center pb-4">
					<h2 class="clr-blue pl-2 pt-2 text-uppercase f40">Questions</h2>
				</div>
			</div>
		</div>
	</section>
	<section class="pt-3 pb-5" id="expertise">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php foreach ($faqs as $key => $faq): ?>
						
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading<?= $faq['id'] ?>">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $faq['id'] ?>" aria-expanded="true" aria-controls="collapse<?= $faq['id'] ?>">
										<?= $faq['question'] ?>
									</a>
								</h4>

							</div>
							<?php if ($key == 0): ?>
							<div id="collapse<?= $faq['id'] ?>" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="heading<?= $faq['id'] ?>">
								<?php else: ?>
								<div id="collapse<?= $faq['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $faq['id'] ?>">
									<?php endif ?>
								<div class="panel-body px-3 px-3"><?= $faq['answer'] ?></div>
							</div>
						</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view('frontend/layouts/footer');?>