<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Other Services Management
			<small>Add Other Services</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/otherservices/list">Other Services</a></li>
			<li class="active">Edit Other Service</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">

						<div class="col-md-12">
							<div class="form-group">
								<label>Service *</label>
								<select name="service_id" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="">Select</option>
									<?php foreach ($services as $service): ?>
									<option value="<?php echo $service['id'] ?>"><?php echo $service['service_title'] ?>										
									</option>
								<?php endforeach ?>									
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Other Sevice Image</label>
								<input type='file'  name="otherservices_image" onchange="readURL(this);" value="<?=$Record['otherservices_image']?>" >
								<?php if (isset($Record['otherservices_image'])): ?>
									<img id="blah" src="<?= base_url('uploads/otherservices')?>/<?=$Record['otherservices_image']?>" alt="your image" class="pre-img" />
									<?php else: ?>
										<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
									<?php endif ?>

								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Other Sevice Title *</label>
									<input type="text" name="otherservices_title" class="form-control" placeholder="Enter Other Sevice Title...." value="<?=$Record['otherservices_title']?>">
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group">
									<label>Description *</label>
									<textarea id="editor1" name='description'><?=$Record['description']?>
								</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Other Sevice Meta Title *</label>
								<input type="text" name="otherservices_metatitle" class="form-control" placeholder="Enter Other Sevice Meta Title...." value="<?=$Record['otherservices_metatitle']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Other Sevice Meta Description *</label>
								<input type="text" name="otherservices_metadesc" class="form-control" placeholder="Enter Other Sevice Meta Description...." value="<?=$Record['otherservices_metadesc']?>">
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Other Sevice Meta Keyword </label>
								<input type="text" name="otherservices_metakeyword" class="form-control" placeholder="Enter Other Sevice Meta Keywords...." value="<?=$Record['otherservices_metakeyword']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Other Sevice Canonical/URL </label>
								<input type="text" name="slug" class="form-control" placeholder="Enter Other Sevice URL...." value="<?=$Record['slug']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Other Sevice Schema</label>
								<input type="text" name="otherservices_schema" class="form-control" placeholder="Enter Other Sevice Schema...." value="<?=$Record['otherservices_schema']?>">
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layouts/footer');?>