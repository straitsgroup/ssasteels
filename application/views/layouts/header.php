	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?=$pagetitle;?> | <?= $this->config->item('app_name') ?></title>
		<link rel="icon" type="image/png" href="<?= base_url()?>assets/backend/dist/img/favicon.png">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/backend/dist/css/custom.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/jvectormap/jquery-jvectormap.css">
		<link rel="stylesheet" href="<?= base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<a href="<?= base_url()?>admin/dashboard" class="logo">
					<span class="logo-mini">
						<img src="<?= base_url()?>assets/backend/dist/img/favicon.png" class="img-responsive" alt="User Image">
					</span>
					<h3><b><?= $this->config->item('app_name') ?></b></h3>

					<!-- <img src="<?= base_url()?>assets/backend/dist/img/logo.png" class="img-responsive" alt="User Image"> -->
				</a>
				<nav class="navbar navbar-static-top">
					<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?= base_url()?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
									<span class="hidden-xs"><?=$this->session->userdata('username')?></span>
								</a>
								<ul class="dropdown-menu" style="min-width: 150px; width: 150px;">
									<li class="user-footer">
										<div class="pull-right">
											<a href="<?=base_url()?>admin/change-password" class="">Change Password</a>
										</div>
									</li>
									<li class="user-footer">
										<div class="pull-right">
											<a href="<?=base_url()?>logout" class="btn btn-warning btn-flat"><i class="fa fa-power-off" style="padding-right: 5px;"></i> Logout</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>