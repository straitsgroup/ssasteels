<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li><a href="<?=base_url()?>admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li><a href="<?=base_url()?>admin/pages/list"><i class="fa fa-file"></i> <span>Pages</span></a></li>
			
			<li><a href="<?=base_url()?>admin/user/list"><i class="fa fa-user"></i> <span>User Managements</span></a></li>
			
			<li><a href="<?=base_url()?>admin/service/list"><i class="fa fa-gear"></i> <span>Services</span></a></li>
			<li><a href="<?=base_url()?>admin/service-component/list"><i class="fa fa-gears"></i> <span>Service Types</span></a></li>
			<li><a href="<?=base_url()?>admin/otherservices/list"><i class="fa fa-gears"></i> <span>Other Services</span></a></li>
			<li><a href="<?=base_url()?>admin/gallery/list"><i class="fa fa-image"></i> <span>Gallery</span></a></li>
			<li><a href="<?=base_url()?>admin/invoice/list"><i class="fa fa-print"></i> <span>Invoice</span></a></li>
			<li><a href="<?=base_url()?>admin/faq/list"><i class="fa fa-print"></i> <span>FAQ</span></a></li>
			
		</ul>
	</section>
</aside>