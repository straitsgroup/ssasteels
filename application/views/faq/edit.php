<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			FAQ Management
			<small>Edit FAQ</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/faq/list">FAQ</a></li>
			<li class="active">Edit FAQ</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>Question *</label>
								<textarea class="form-control" id="editor1" name='question' rows="5" placeholder="Question"><?=$Record['question']?></textarea>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label>Answer *</label>
								<textarea class="form-control" id="editor2" name='answer' rows="5" placeholder="Answer"><?=$Record['answer']?></textarea>
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;" required>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
</div>
<?php $this->load->view('layouts/footer');?>