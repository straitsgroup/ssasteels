<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Invoice Management
			
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/invoice/list">Invoice List</a></li>
			<li class="active">Add Invoice</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<?php if($this->session->flashdata('msg')): ?>
					<div class="alert alert-info">
						<strong>Info!</strong> <?php echo $this->session->flashdata('msg') ?>
					</div>
				<?php endif ?>
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						

						<div class="col-md-12">
							<div class="form-group">
								<label>Customer Name *</label>
								<input type="text" name="customer_name" class="form-control" placeholder="Enter Customer Name" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Customer Phone No. *</label>
								<input type="text" name="customer_phone" class="form-control" placeholder="Enter Customer Phone Number" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Building Width (In Meter) *</label>
								<input type="text" name="building_width" class="form-control" placeholder="Enter Building Width" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Building Length (In Meter) *</label>
								<input type="text" name="building_length" class="form-control" placeholder="Enter Building Length" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Building Height (In Meter) *</label>
								<input type="text" name="building_height" class="form-control" placeholder="Enter Building Height" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Location *</label>
								<input type="text" name="location" class="form-control" placeholder="Enter Location" required>
							</div>
						</div>
						
						
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('layouts/footer');?>